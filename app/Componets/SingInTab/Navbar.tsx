
"use client";
import { useState } from 'react';
import Image from 'next/image';
import dots from '/public/assets/dots-menu.png';
import bell from '/public/assets/bell.png';
import settings from '/public/assets/settings.png';

import Sidebar from './Sidebar';
import Topbar from './Topbar';
export default function Navbar() {
    const [getValue, setValue] = useState('')
    return (
        <div>
            <nav className='flex items-center justify-between flex-wrap bg-black p-3 '>
                <div className="flex items-center space-x-4">
                    <Image src={dots} alt="Logo" className="h-2 w-2" /> {/* Adjust height and width as needed */}
                    <span className="text-xs  text-red-500 font-sans">Electronic Art</span>
                    <span className="text-xs mx-32  text-white font-sans">Outlook</span>
                    <div className="items-center px-1 flex justify-start" >
                        <div className="relative mr-3">
                            <div className="absolute top-2 left-3 items-center" >
                                <svg className="w-5 h-5  text-gray-500" fill="currentColor" viewBox="0 0 25 25" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                            </div>
                            <input
                                type="text"
                                className="block p-2 pl-8 w-70 h-8 text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:pl-3"
                                placeholder="Search Here..."
                            />
                        </div>
                    </div>

                </div>
                <div className="flex items-center space-x-4">
                    <Image src={dots} alt="Logo" className="h-4 w-4" />
                    <Image src={bell} alt="Logo" className="h-4 w-4" />
                    <Image src={settings} alt="Logo" className="h-4 w-4" />
                    <Image src={dots} alt="Logo" className="h-4 w-4" />
                    <Image src={bell} alt="Logo" className="h-4 w-4" />
                    <Image src={settings} alt="Logo" className="h-4 w-4" />
                    <Image src={dots} alt="Logo" className="h-4 w-4" />

                </div>

            </nav>
            <div className='flex items-start justify-start'>
            <Sidebar/>
            <Topbar/>
            </div>
        </div>
    )
}
