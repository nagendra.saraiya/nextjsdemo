import React from 'react'
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';


export default function EventsCalendra() {
  return (
    <div className="container px-4 mt-5 ">
    <div className="bg-white p-4 rounded-md shadow-md">
      <FullCalendar plugins={[dayGridPlugin]} initialView="dayGridMonth" />
    </div>
  </div>
)
}
