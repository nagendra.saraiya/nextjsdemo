"use client";
import React, { useState } from 'react'
import EventsCalendra from './EventsCalendra';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
type ValuePiece = Date | null;
type Value = ValuePiece | [ValuePiece, ValuePiece];
export default function Calendra() {

    const [value, onChange] = useState<Value>(new Date());
    return (
        <div className="flex bg-gray-100">
            <div className='mt-5 mx-8'>
                <Calendar onChange={onChange} value={value} 
                />
            </div>
            <EventsCalendra />
        </div> 
    )
}
