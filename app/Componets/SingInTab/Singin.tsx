import { useState } from "react";
import { FaFacebook, FaLinkedinIn, FaGoogle, FaRegEnvelope } from "react-icons/fa";
import { MdLockOutline } from "react-icons/md";
//import { useRouter } from "next/router"


export default function Singin() {
    //const navigate = useRouter()
    const [email, setEmail] = useState('');
    //const [password, setPassword] = useState('');
  
    return (
      <div className="flex flex-col items-center justify-center min-h-screen p-6 md:p-24 bg-gray-100">
        <div className="bg-white rounded-2xl shadow-2xl flex flex-col md:flex-row w-full md:w-2/3 max-w-4xl">
          <div className="w-full md:w-4/5 p-5">
            <div className="text-center md:text-left font-bold">
              <span className="text-green-500">Company</span>Name
            </div>
            <div className="py-10">
              <h2 className="text-center font-bold text-green-500 mx-2 md:mx-20">Sign in to Account</h2>
              <div className="flex justify-center items-center py-2">
              <div className="border-2 w-10 border-green-500"></div>
              </div>
              <div className="flex justify-center md:justify-center my-2">
                <a href="#" className="border-2 border-gray-200 rounded-full p-3 mx-1">
                  <FaFacebook className="text-sm" />
                </a>
                <a href="#" className="border-2 border-gray-200 rounded-full p-3 mx-1">
                  <FaLinkedinIn className="text-sm" />
                </a>
                <a href="#" className="border-2 border-gray-200 rounded-full p-3 mx-1">
                  <FaGoogle className="text-sm" />
                </a>
              </div>
              <p className="text-gray-400 mx-2 md:mx-20 my-3 text-center">or use your email account</p>
              <div className="bg-gray-100 w-full md:w-64 p-2 flex items-center mx-auto md:mx-14 mb-3">
                <FaRegEnvelope className="text-gray-400 m-2" />
                <input type="email" name="email" placeholder="Email" className="bg-gray-100 outline-none text-sm flex-1" />
              </div>
              <div className="bg-gray-100 w-full md:w-64 p-2 flex items-center mx-auto md:mx-14 mb-3">
                <MdLockOutline className="text-gray-400 m-2" />
                <input type="password" name="password" placeholder="Password" className="bg-gray-100 outline-none text-sm flex-1" />
              </div>
              <div className="flex justify-between w-full md:w-64 mb-5 mx-auto md:mx-14">
                <label className="flex items-center text-xs">
                  <input type="checkbox" name="remember" className="mr-1" />Remember Me
                </label>
                <a href="#" className="text-xs">Forgot Password?</a>
              </div>
              <div className="flex justify-center items-center">
              <div className="border-2 border-green-500 text-green-500 rounded-full px-12 py-2 font-semibold hover:bg-green-500 hover:text-white">
                Signup
              </div>
            </div>
            </div>
          </div>
          <div className="w-full md:w-2/5 bg-green-500 text-white rounded-tr-2xl rounded-br-2xl py-12 md:py-36 px-6 md:px-12">
            <h2 className="text-center font-bold mb-2">Hello, Friends!</h2>
            <div className="flex justify-center items-center py-2">
            <div className="border-2 w-10 border-white"></div>
            </div>
            <p className="mb-5 mx-1 md:mx-0 line-clamp-2 text-center">Fill up personal information and start journey with us</p>
            <div className="flex justify-center items-center">
              <div className="border-2 border-white rounded-full px-12 py-2 font-semibold hover:bg-white hover:text-green-500">
                Signup
              </div>
            </div>
  
          </div>
        </div>
      </div>
    );
  }
  