"use client";

import Image from 'next/image'
import React, { useState } from 'react'
import bell from '/public/assets/bell.png';
import calendra from '/public/assets/calendarWhite.png';

import downarrow from '/public/assets/down-arrow.png';
import clendarday from '/public/assets/calendarday.png';
import downarrowblack from '/public/assets/down-arrowblack.png';
import sort from '/public/assets/sort.png';
import printer from '/public/assets/printer.png';
import link from '/public/assets/link.png';
import planning from '/public/assets/planning.png';
import menu from '/public/assets/menu.png';
import Calendra from './Calendra';


export default function Topbar() {
    const [activeTab, setActiveTab] = useState(1);

    const tabs = [
        { id: 1, label: 'Home' },
        { id: 2, label: 'View' },
        { id: 3, label: 'Help' },
      ];
      
    return (
        <div className='bg-gray-100'>
            <div className='h-5 w-auto flex mt-4 mx-2 rounded-sm'>
                <div className=" flex items-center">
                    <Image src={menu} alt="Logo" className="h-3 w-3 mx-2" />
                    {tabs.map(tab => (
                    <div key={tab.id}
                    className={`${
                        activeTab === tab.id
                          ? 'border-b-2 border-blue-500'
                          : 'border-b border-transparent'
                      } text-gray-600 text-xs hover:text-gray-800 px-4 py-2 focus:outline-none`}
                      onClick={() => setActiveTab(tab.id)}
            
          >          {tab.label}
          </div>
                    ))}

                </div>

            </div>
        <div className="bg-gray-200 h-9 w-auto text-white shadow-2xl mt-5 mx-3 rounded-lg w-70 flex">


            <div className='h-5 w-auto bg-[#0096FF] flex mt-2 mx-1 rounded-sm'>
                <div className=" flex items-center">
                    <Image src={calendra} alt="Logo" className="h-3 w-3 mx-2" />
                    <div className='text-white text-xs'>New Event </div>
                    <div className='bg-white h-5  w-px mx-2'></div>

                    <Image src={downarrow} alt="Logo" className="h-3 w-3 mx-1" />

                </div>

            </div>
            <div className='h-5 w-auto  flex mt-2 mx-1 rounded-sm'>
                <div className=" flex items-center">
                    <Image src={clendarday} alt="Logo" className="h-3 w-3 mx-2" />
                    <div className='text-gray-900 text-xs'>Day </div>

                    <Image src={downarrowblack} alt="Logo" className="h-3 w-3 mx-1" />

                </div>

            </div>

            <div className='h-5 w-auto  flex mt-2 mx-1 rounded-sm'>
                <div className=" flex items-center">
                    <Image src={planning} alt="Logo" className="h-3 w-3 mx-2" />
                    <div className='text-gray-900 text-xs'>Work Week </div>
                </div>

            </div>

            <div className='h-5 w-auto  flex mt-2 mx-1 rounded-sm'>
                <div className=" flex items-center">
                    <Image src={planning} alt="Logo" className="h-3 w-3 mx-2" />
                    <div className='text-gray-900 text-xs'>Week </div>
                </div>

            </div>

            <div className='h-5 w-auto bg-gray-300 py-2 shadow-2xl flex mt-2 mx-1 rounded-sm'>
                <div className=" flex items-center">
                    <Image src={clendarday} alt="Logo" className="h-3 w-3 mx-2" />
                    <div className='text-gray-900 text-xs mr-2'>Month </div>

                </div>

            </div>
            <div className='h-5 w-auto  flex mt-2 mx-1 rounded-sm'>
                <div className=" flex items-center">
                    <Image src={clendarday} alt="Logo" className="h-3 w-3 mx-2" />
                    <div className='text-gray-400 text-xs'>Split View </div>
                </div>

            </div>

            <div className='bg-gray-300 h-6  w-px mx-2 mt-2'></div>

            <div className='h-5 w-auto  flex mt-2 mx-1 rounded-sm'>
                <div className=" flex items-center">
                    <Image src={sort} alt="Logo" className="h-3 w-3 mx-2" />
                    <div className='text-gray-900 text-xs'>Filter </div>

                    <Image src={downarrowblack} alt="Logo" className="h-3 w-3 mx-1" />

                </div>

            </div>

            <div className='bg-gray-300 h-6  w-px mx-2 mt-2'></div>

            <div className='h-5 w-auto  flex mt-2 mx-1 rounded-sm'>
                <div className=" flex items-center">
                    <Image src={link} alt="Logo" className="h-3 w-3 mx-2" />
                    <div className='text-gray-900 text-xs'>Share </div>
                </div>

            </div>


            <div className='h-5 w-auto  flex mt-2 mx-1 rounded-sm mr-32'>
                <div className=" flex items-center">
                    <Image src={printer} alt="Logo" className="h-3 w-3 mx-2" />
                    <div className='text-gray-900 text-xs'>Print </div>
                </div>

            </div>


            </div>
            <Calendra/>

        </div>
    )
}
