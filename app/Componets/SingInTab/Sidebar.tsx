import React from 'react'
import Image from 'next/image';
import mail from '/public/assets/mail.png';
import contact from '/public/assets/contact.png';
import check from '/public/assets/check.png';
import dotImage from '/public/assets/dots.png';
import calendar from '/public/assets/calendar.png';



export default function Sidebar() {
  return (

    <div className="h-screen bg-gray-200  text-white w-12 p-4 shadow-2xl flex justify-center">
        <div className="flex-col items-center space-y-4">
            <Image src={mail} alt="Logo" className="h-4 w-4 mt-2 "/>
            <Image src={calendar} alt="Logo" className="h-4 w-4 mt-2  "/>
            <Image src={contact} alt="Logo" className="h-4 w-4 mt-2 "/>
            <Image src={contact} alt="Logo" className="h-4 w-4 mt-2 "/>
            <Image src={check} alt="Logo" className="h-4 w-4 mt-2 "/>
            <Image src={contact} alt="Logo" className="h-4 w-4 mt-2 "/>
            <Image src={dotImage} alt="Logo" className="h-4 w-4 mt-2 "/>
        </div>
    </div>
)
}
